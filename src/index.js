import Phaser from 'phaser';

import Difficulty from './scenes/Difficulty';
import Game from './scenes/Game';
import Menu from './scenes/Menu';
import Over from './scenes/Over';
import Scoreboard from './scenes/Scoreboard';

const config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    scene: [
        Difficulty,
        Game,
        Menu,
        Over,
        Scoreboard
    ]
};

const game = new Phaser.Game(config);

game.scene.start('Menu');
