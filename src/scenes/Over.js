import Phaser from 'phaser';

import systeme from '../models/Systeme';
import gameOver from '../assets/game-over.png';

class Over extends Phaser.Scene {

    constructor() {
        super({
            key: 'Over'
        });
    }

    preload() {
        this.load.image('game-over', gameOver);
    }

    create() {
        this.cameras.main.setBackgroundColor('#fed420');

        this.add.sprite(400, 300, 'game-over');

        setTimeout(() => this.askForName(), 2000);

    }

    async askForName() {
        const name = prompt("Entrez votre nom") || "Anonyme";
        await systeme.entreNom(name);
        this.scene.start('Menu');
    }

}

export default Over;
