import Phaser from 'phaser';

import systeme from '../models/Systeme';
import backButton from '../assets/back-button.jpg';

class Scoreboard extends Phaser.Scene {

    constructor() {
        super({
            key: 'Scoreboard'
        });
    }

    preload() {
        this.load.image('back-button', backButton);
    }

    async create() {
        this.cameras.main.setBackgroundColor('#fed420');

        const scores = await systeme.getScores();
        const length = Math.min(5, scores.length);

        for (let i = 0; i < length; i++) {

            const text = `${i + 1} - ${scores[i].name} (${scores[i].score})`;
            const label = this.add.text(400, 128 + (64 * i), text, {
                fontFamily: 'Arial, Helvetica, sans-serif',
                fontSize: i === 0 ? '48px' : '32px',
                color: '#000'
            });

            label.setOrigin(0.5);

        }

        const back = this.add.sprite(400, 500, 'back-button');
        back.setInteractive({ useHandCursor: true });
        back.on('pointerdown', () => this.goToMenu());

    }

    goToMenu() {
        this.scene.start('Menu');
    }

}

export default Scoreboard;
