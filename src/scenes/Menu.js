import Phaser from 'phaser';

import backgroundYellow from '../assets/background-yellow.jpg';
import playButton from '../assets/play-button.jpg';
import scoresButton from '../assets/scores-button.jpg';
import title from '../assets/title.png';

class Menu extends Phaser.Scene {

    constructor() {
        super({
            key: 'Menu'
        });
    }

    preload() {
        this.load.image('background-yellow', backgroundYellow);
        this.load.image('play-button', playButton);
        this.load.image('scores-button', scoresButton);
        this.load.image('title', title);
    }

    create() {
        this.cameras.main.setBackgroundColor('#fed420');

        this.add.sprite(400, 300, 'background-yellow');
        this.add.sprite(250, 180, 'title');
        const play = this.add.sprite(250, 400, 'play-button');
        const scores = this.add.sprite(250, 480, 'scores-button');

        play.setInteractive({ useHandCursor: true });
        play.on('pointerdown', () => this.goToDifficulty());

        scores.setInteractive({ useHandCursor: true });
        scores.on('pointerdown', () => this.goToScoreboard());
    }

    goToDifficulty() {
        this.scene.start('Difficulty');
    }

    goToScoreboard() {
        this.scene.start('Scoreboard');
    }

}

export default Menu;
