import Phaser from 'phaser';

import systeme from '../models/Systeme';
import donut from '../assets/donut.png';
import duffBeer from '../assets/duff-beer.png';
import homer from '../assets/homer.png';
import restartButton from '../assets/restart-button.png';

class Game extends Phaser.Scene {

    beers = [];
    shouldWait = true;

    constructor() {
        super({
            key: 'Game'
        });
    }

    preload() {
        this.load.image('donut', donut);
        this.load.image('duff-beer', duffBeer);
        this.load.image('homer', homer);
        this.load.image('restart-button', restartButton);
    }

    create() {
        this.cameras.main.setBackgroundColor('#fed420');

        this.homer = this.add.sprite(0, 0, 'homer');
        this.homer.setVisible(false);

        this.beers[0] = this.add.sprite(150, 300, 'duff-beer');
        this.beers[0].setZ(10);
        this.beers[0].setInteractive({ useHandCursor: true });
        this.beers[0].on('pointerdown', () => this.chooseBeer(0));

        this.beers[1] = this.add.sprite(400, 300, 'duff-beer');
        this.beers[1].setZ(11);
        this.beers[1].setInteractive({ useHandCursor: true });
        this.beers[1].on('pointerdown', () => this.chooseBeer(1));

        this.beers[2] = this.add.sprite(650, 300, 'duff-beer');
        this.beers[2].setZ(12);
        this.beers[2].setInteractive({ useHandCursor: true });
        this.beers[2].on('pointerdown', () => this.chooseBeer(2));

        this.scoreText = this.add.text(20, 20, `Score: 0`, {
            fontFamily: 'Arial, Helvetica, sans-serif',
            fontSize: '32px',
            color: '#000'
        });

        this.restartBtn = this.add.sprite(690, 50, 'restart-button');
        this.restartBtn.setInteractive({ useHandCursor: true });
        this.restartBtn.on('pointerdown', () => this.restartTour());

        this.donutImg = this.add.sprite(750, 50, 'donut');

        systeme.commencerPartie();

        this.startTour();
    }

    startTour() {
        this.shouldWait = true;
        this.updateScore();
        this.updateDonut();

        const initialPosition = systeme.getPositionInitiale();

        this.homer.setPosition((250 * initialPosition) + 150, 300);
        this.homer.setVisible(true);

        this.tweens.add({
            targets: this.beers[initialPosition],
            y: -100,
            duration: 1000,
            yoyo: true,
            ease: 'Power2',
            onComplete: () => this.startMixupAnimations()
        });

    }

    startMixupAnimations() {

        const mixups = systeme.getMelanges();
        const mixupDuration = systeme.getDureeMelange();

        this.homer.setVisible(false);

        this.swapAnimation(mixups, 0, mixupDuration, this.onMixupEnd.bind(this));

    }

    swapAnimation(mixups, i, duration, cb) {

        const b0 = this.beers[mixups[i][0]];
        const b1 = this.beers[mixups[i][1]];
        const x0 = (250 * mixups[i][0]) + 150;
        const x1 = (250 * mixups[i][1]) + 150;

        this.tweens.add({
            targets: b0,
            x: x1,
            duration: duration,
            onComplete: () => {
                b0.setPosition(x0, 300);

                if (i < mixups.length - 1) {
                    this.swapAnimation(mixups, i + 1, duration, cb);
                } else {
                    cb();
                }
            }
        });

        this.tweens.add({
            targets: b1,
            x: x0,
            duration: duration,
            onComplete: () => {
                b1.setPosition(x1, 300);
            }
        });
    }

    onMixupEnd() {
        this.shouldWait = false;
    }

    restartTour() {
        if (this.shouldWait) return;
        this.shouldWait = true;

        if (systeme.recommencerTour()) {
            this.startTour();
        }

    }

    chooseBeer(position) {
        if (this.shouldWait) return;
        this.shouldWait = true;

        const isRightChoice = systeme.choixBiere(position);

        if (isRightChoice) {
            this.homer.setPosition((250 * position) + 150, 300);
            this.homer.setVisible(true);
        }

        this.tweens.add({
            targets: this.beers[position],
            y: -100,
            duration: 1000,
            yoyo: true,
            ease: 'Power2',
            onComplete: () => {

                this.homer.setVisible(false);

                if (isRightChoice) {
                    this.startTour();
                } else {
                    this.scene.start('Over');
                }

            }
        });

    }

    updateScore() {
        this.scoreText.setText(`Score: ${systeme.getScore()}`);
    }

    updateDonut() {
        this.donutImg.setVisible(systeme.getPeutRecommencer());
        this.restartBtn.setVisible(systeme.getPeutRecommencer());
    }

}

export default Game;
