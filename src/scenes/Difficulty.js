import Phaser from 'phaser';

import systeme from '../models/Systeme';
import backButton from '../assets/back-button.jpg';
import easyButton from '../assets/easy-button.jpg';
import hardButton from '../assets/hard-button.jpg';
import normalButton from '../assets/normal-button.jpg';

class Difficulty extends Phaser.Scene {

    constructor() {
        super({
            key: 'Difficulty'
        });
    }

    preload() {
        this.load.image('back-button', backButton);
        this.load.image('easy-button', easyButton);
        this.load.image('hard-button', hardButton);
        this.load.image('normal-button', normalButton);
    }

    create() {
        this.cameras.main.setBackgroundColor('#fed420');

        const easy = this.add.sprite(400, 200, 'easy-button');
        easy.setInteractive({ useHandCursor: true });
        easy.on('pointerdown', () => this.goToGame('facile'));

        const normal = this.add.sprite(400, 280, 'normal-button');
        normal.setInteractive({ useHandCursor: true });
        normal.on('pointerdown', () => this.goToGame('normal'));

        const hard = this.add.sprite(400, 360, 'hard-button');
        hard.setInteractive({ useHandCursor: true });
        hard.on('pointerdown', () => this.goToGame('difficile'));

        const back = this.add.sprite(400, 440, 'back-button');
        back.setInteractive({ useHandCursor: true });
        back.on('pointerdown', () => this.goToMenu());
    }

    goToMenu() {
        this.scene.start('Menu');
    }

    goToGame(difficulty) {
        systeme.setDifficulte(difficulty);
        this.scene.start('Game');
    }

}

export default Difficulty;
