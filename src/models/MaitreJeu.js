class MaitreJeu {

    bieres = [0, 1, 2];

    /**
     * Choisit un emplacement aléatoire pour Homer entre 0 et 2.
     * @returns {number}
     */
    cacheHomer() {
        return this.getRandomInArray(this.bieres);
    }

    /**
     * Génère une suite aléatoire de 6 à 18 mélanges de bières.
     * @returns {[]}
     */
    melangeBieres() {

        const melanges = [];
        const nombreDeMelanges = this.getRandomBetween(6, 18);

        for (let i = 0; i < nombreDeMelanges; i++) {

            let biere1 = this.getRandomInArray(this.bieres);
            let biere2;

            do {
                biere2 = this.getRandomInArray(this.bieres);
            } while (biere1 === biere2);

            melanges.push([biere1, biere2]);

        }

        return melanges;
    }

    /**
     * Utilitaire pour obtenir un élément aléatoire dans un tableau.
     * Cette méthode est privée.
     * @param arr
     * @returns {*}
     */
    getRandomInArray(arr) {
        return arr[Math.floor(Math.random() * arr.length)];
    }

    /**
     * Utilitaire pour obtenir un nombre aléatoire entre deux bornes.
     * Cette méthode est privée.
     * @param min
     * @param max
     * @returns {number}
     */
    getRandomBetween(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

}

export default MaitreJeu;
