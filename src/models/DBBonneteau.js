import Dexie from 'dexie';

class DBBonneteau {

    database;

    /**
     * Constructeur de la base de données IndexedDB.
     */
    constructor() {
        this.database = new Dexie('DBBonneteau');

        this.database.version(1).stores({
            scores: '++id, name, score'
        });
    }

    /**
     * Permet de sauver un Score dans la base de données.
     * @param score
     * @returns {Promise<void>}
     */
    async saveScore(score) {
        await this.database.scores.add(score);
    }

    /**
     * Getter pour récupérer les scores sauvés, ordrés par le
     * plus haut score en premier.
     * @returns {Promise<T[]|any>}
     */
    async getScores() {
        return this.database.scores.reverse().sortBy('score');
    }

}

export default DBBonneteau;
