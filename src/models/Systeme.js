import DBBonneteau from './DBBonneteau';
import MaitreJeu from './MaitreJeu';
import Score from './Score';

const durees = new Map();
durees.set('facile', 1000);
durees.set('normal', 600);
durees.set('difficile', 300);

class Systeme {

    maitre;
    database;

    score = 0;
    difficulte = 'normal';
    peutRecommencer = true;
    positionInitiale = 0;
    melanges = [];

    /**
     * Constructeur du système.
     */
    constructor() {
        this.maitre = new MaitreJeu();
        this.database = new DBBonneteau();
    }

    /**
     * Commencer une nouvelle partie.
     * Les variables sont réinitialisées.
     */
    commencerPartie() {
        this.score = 0;
        this.peutRecommencer = true;
        this.lancerTour();
    }

    /**
     * Permet de recommencer un tour, et donc de remélanger les
     * bières. Le joueur ne peut utiliser cette fonctionnalité
     * qu'une seule fois par partie.
     * @returns {boolean} true si le joueur pouvait recommencer.
     */
    recommencerTour() {

        if (this.peutRecommencer) {
            this.peutRecommencer = false;
            this.lancerTour();
            return true;
        }

        return false;
    }

    /**
     * Le joueur choisit la bière derrière laquelle il pense
     * que Homer se cache. Si c'est correct, un nouveau tour
     * commence.
     * @param position: nombre entre 0 et 2
     * @returns {boolean} true si son choix est correct.
     */
    choixBiere(position) {

        if (position === this.getPositionFinale()) {
            this.score++;
            this.lancerTour();
            return true;
        }

        return false;
    }

    /**
     * Lance un nouveau tour. La nouvelle position de Homer est
     * choisie aléatoirement et les mélanges de bières sont ensuite
     * générés.
     * Cette méthode est privée.
     */
    lancerTour() {
        this.positionInitiale = this.maitre.cacheHomer();
        this.melanges = this.maitre.melangeBieres();
    }

    /**
     * Lorsque le joueur a terminé une partie, il peut entrer
     * son nom pour sauver son score dans la base de données.
     * @param nom
     * @returns {Promise<void>}
     */
    async entreNom(nom) {
        await this.database.saveScore(new Score(nom, this.score));
    }

    /**
     * Getter pour le score actuel du joueur.
     * @returns {number}
     */
    getScore() {
        return this.score;
    }

    /**
     * Getter pour la position initiale de Homer.
     * @returns {number}
     */
    getPositionInitiale() {
        return this.positionInitiale;
    }

    /**
     * Getter pour les mélanges générés pour ce tour.
     * @returns {[]}
     */
    getMelanges() {
        return this.melanges;
    }

    /**
     * Getter pour savoir si le joueur a encore le droit
     * de recommencer un tour durant cette partie.
     * @returns {boolean}
     */
    getPeutRecommencer() {
        return this.peutRecommencer;
    }

    /**
     * Getter pour connaître la durée d'un swap entre deux bières.
     * @returns {number} milliseconds
     */
    getDureeMelange() {
        return durees.get(this.difficulte) / (this.score / 10 + 1);
    }

    /**
     * Getter pour la position finale de Homer, par rapport
     * à sa position initiale et suite aux mélanges.
     * Cette méthode est privée.
     * @returns {number}
     */
    getPositionFinale() {
        let positionFinale = this.positionInitiale;
        for (let i = 0; i < this.melanges.length; i++) {
            if (positionFinale === this.melanges[i][0]) {
                positionFinale = this.melanges[i][1];
            } else if (positionFinale === this.melanges[i][1]) {
                positionFinale = this.melanges[i][0];
            }
        }
        return positionFinale;
    }

    /**
     * Getter pour les scores de la base de données.
     * @returns List<Score>
     */
    async getScores() {
        return this.database.getScores();
    }

    /**
     * Setter pour la difficulté de la partie.
     * @param difficulte: facile, normal ou difficile
     */
    setDifficulte(difficulte) {
        this.difficulte = difficulte;
    }

}

export default new Systeme();
