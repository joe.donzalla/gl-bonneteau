class Score {

    name;
    score;

    /**
     * Constructeur permettant de créer un nouveau score lié à un nom.
     * @param name
     * @param score
     */
    constructor(name, score) {
        this.name = name;
        this.score = score;
    }

    /**
     * Getter for the name
     * @returns {string}
     */
    getName() {
        return this.name;
    }

    /**
     * Getter for the score
     * @returns {integer}
     */
    getScore() {
        return this.score;
    }

}

export default Score;
